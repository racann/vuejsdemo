window.Event = new class {
    constructor() {
        this.vue = new Vue();
    }

    fire(event, data=null){
        this.vue.$emit(event, data);
    }


    listen(event, calback){
        this.vue.$on(event, calback);
    }
}


Vue.component('coupon', {
    template: '<input placeholder="Enter your coupon code" @blur="onCouponApplied">',
    methods: {
        onCouponApplied() {
            Event.fire('applied');
        }
    }
})

Vue.component('task-list', {
    template: '<div><task v-for="task in tasks">{{task.task}}</task></div>',

    data() {
        return {
            tasks:[
                {task:'Go to the bank', completed:true},
                {task:'Exercise', completed:true},
                {task:'Watch Walking Dead', completed:false},
            ]
        };
    }
});
Vue.component('task', {
    template: '<li><slot></slot></li>',

    data() {
        return {
            message: "foobar"
        };
    }
});

Vue.component('tabs', {
   template:`
   <div>
   <div class="tabs">
  <ul>
  <li v-for="tab in tabs" :class="{ 'is-active': tab.isActive }"><a :href="tab.href" @click="selectTab(tab)">{{tab.name}}</a></li>
</ul>
</div>

   <div class="tabs-details">
   <slot></slot>
</div>
</div>
   `,
    data() {
      return {tabs: []}
    },
    created(){
        this.tabs = this.$children;
    },
    methods: {
      selectTab(selectedTab){
          this.tabs.forEach(tab => {
              tab.isActive = (tab.name == selectedTab.name);
          });
      }
    }
});

Vue.component('tab', {
    template: `
    <div v-show="isActive"><slot></slot></div>
    `,
    props: {
        name: {required: true},
        selected: {default: false}
    },
    data() {
        return{
            isActive: false
        }
    },
    computed: {
      href() {
          return '#' + this.name.toLowerCase().replace(/ /g, '-');
      }
    },
    mounted(){
        this.isActive = this.selected;
    }
})

Vue.component('modal', {
   template: `
 <div class="modal is-active">
  <div class="modal-background"></div>
  <div class="modal-card">
    <header class="modal-card-head">
      <p class="modal-card-title"><slot name="header"></slot></p>
      <button class='delete' aria-label='close' @click="$emit('close')"></button>
    </header>
    <section class="modal-card-body">
      <slot>Default content here.</slot>
    </section>
    <footer class="modal-card-foot">
    <slot name="footer">
        <button class="button is-primary">Ok</button>
    </slot>
    </footer>
  </div>
</div>
   `
});

Vue.component('message', {
    props: ['title', 'body'],
    data(){
      return {
          isVisible: true
      }
    },
    template: `
<div>
    <h1>Message Componenet</h1> 
    <article class='message' v-show="isVisible"> 
    <div class='message-header'> 
    {{title}} 
    <button type="button" @click="hideModal">X</button>
    </div> 
    <div class='message-body'> {{body}}</div> 
    </article>
</div>
`,
    methods: {
        hideModal(){
            this.isVisible = false;
        }
    }
});

new Vue({
    el: '#root',
    data: {
        message: "Hello World",
        title: "Now the title is being set through Javascript",
        showLogIn: false,
        isLoading: false,
        couponApplied: false,
        names:['Joe', 'Mary', 'John', 'Jack'],
        tasks: [
            {description: 'Go to the store', completed: false},
            {description: 'Eat food', completed: true},
            {description: 'Watch punisher', completed: false}
        ]

    },
    computed: {
        reversedMessage: function(){
            return this.message.split('').reverse().join('');
        },
        incompleteTasks: function(){
            return this.tasks.filter(task => ! task.completed);
        },
        completeTasks: function(){
            return this.tasks.filter(task => task.completed);
        }
    },
    methods: {
        addName: function(){
            this.names.push(this.message);
            this.message="";
        },
        toggleClass: function(){
            this.isLoading = true;
        }
    },
    created() {
        Event.listen('applied', () => alert('Handling it'))
    }
});

